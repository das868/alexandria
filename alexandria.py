#!/usr/bin/env python3

import os


def rahmenmacher(string):
    string = "| " + string + " |"
    rand = "·" + (len(string) - 2) * "–" + "·"
    os.system("clear")
    # ALT:   print(rand, "\n", string, "\n", rand, sep="", end="\n\n")
    return rand + "\n" + string + "\n" + rand + "\n\n"


def hauptmenu():
    print(rahmenmacher("Willkommen zu Alexandria, Ihrer pragmatischen Bibliotheksverwaltung!"))
    #print("·––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––·\n",
    #    "\b| Willkommen zu Alexandria, Ihrer pragmatischen Bibliotheksverwaltung! |\n",
    #        "\b·––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––·\n")

    print("Hauptmenü\n",
            "\b\t1:\tNeues Buch einpflegen\n",
            "\b\t2:\tBestand nach bestimmtem Buch durchsuchen\n",
            "\b\t3:\tGesamten Bestand anzeigen (möglicherweise sehr screenfüllend)\n",
            "\b\t4:\tBestimmtes Buch löschen\n",
            "\b\t5:\tBeenden\n")
    
    wahl = None
    try:
        wahl = int(input("\n\tIhre Wahl:\t\t"))
    except:
        print("\nERROR! Fehlerhafte Eingabe oder Unterbrechung.\n")

    if(wahl == 1):
        rahmenmacher("Neue Bücher einpflegen.")
        wahl = input("\nEin einzelnes (E) oder gleich mehrere (M) auf einen Schlag?\t")

        if wahl == 'E':
            add_buch()

        elif wahl == 'M':
            while True:
                add_buch()
                abbr = input("\nNoch eins? (J/n)\t")
                if abbr.lower() == 'n':
                    break
        
        else:
            print("Fehlerhafte Eingabe, Programmende")
            exit()

    elif(wahl == 2):
        such_buch()
    elif(wahl == 3):
        zeige_bestand()
    elif(wahl == 4):
        del_buch()
    elif(wahl == 5):
        print("Programm beendet.\n")
        exit()
    else:
        # doppelt gemoppelt:    print("ERROR! Programmfehler.")
        exit()


def add_buch():
    autor = input("\nAutor (zuerst Vor-, dann Nachname):\t")
    titel = input("Titel:\t\t\t\t\t")
    verlag = input("Verlag:\t\t\t\t\t")
    ejahr = input("Erscheinungsjahr (v. Chr. = -):\t\t")
    genre = input("Genre:\t\t\t\t\t")
    sprache = input("Sprache(n):\t\t\t\t")

    try:
        datei = open("libfile.txt", "a+")
        #datei.write(autor + "|" + titel + "|" + verlag + "|" + ejahr + "|" + genre + "|" + sprache + "|" + str(refno) + "\n")
        datei.write(autor + "|" + titel + "|" + verlag + "|" + ejahr + "|" + genre + "|" + sprache + "\n")
        datei.flush()
        print("Buch erfolgreich hinzugefügt")
    finally:
        datei.close()
    input("\nENTER drücken zum Fortfahren")
    hauptmenu()


def such_buch():
    print(rahmenmacher("Erweiterte Suche:"))
    try:
        datei = open("libfile.txt", "r+")
        inhalt = datei.readlines()
        inhalt = [x.strip() for x in inhalt] # for end-whitespaces and '\n' removal ;)
        
        infos = list()
        for x in range(0,len(inhalt)):
            eintrag = (inhalt[x].split('|', maxsplit=5)) # vorher maxsplit=6 wegen refno!!!
            infos.append(eintrag)

        autoren, titel, verlage, ejahre, genres, sprachen = [], [], [], [], [], []
        for x in infos:
            autoren.append(x[0])
            titel.append(x[1])
            verlage.append(x[2])
            ejahre.append(x[3])
            genres.append(x[4])
            sprachen.append(x[5])
        # print(autoren, titel, verlage, ejahre, genres, sprachen, sep="\n", end="\n")
        
        # print("Bitte sicherheitshalber nur einen Suchbegriff (am besten den Titel) eingeben, und das genau wie in der Bestandsübersicht!")
        titel_such = input("Genauen Titel eingeben:\t\t")
        if titel_such == "":
            wahl = input("Noch die anderen Infos eingeben? (J/n)\t")
            if wahl == 'J':
                #autor_such = input("Autor(en) eingeben:\t\t") # nicht sinnvoll
                #verlag_such = input("genauen Verlag eingeben:\t") # nicht sinnvoll
                ejahr_such = input("Erscheinungsjahr eingeben:\t") 
                #genre_such = input("Genre eingeben:\t\t\t") # nicht sinnvoll
                #sprache_such = input("Sprache(n) eingeben:\t\t") # nicht sinnvoll
                #print("", autor_such, titel_such, verlag_such, ejahr_such, genre_such, sprache_such, sep="\n", end="\n")

        gefunden = False
        for x in infos:
            try:
                gesucht = x.index(titel_such) # stupid stub just for checkup
                gefunden = True
                print("\nAutor:\t\t\t %s\nTitel:\t\t\t %s\nVerlag:\t\t\t %s\nErscheinungsjahr:\t %s\n"
                        % (x[0], x[1], x[2], x[3]), end="")
                print("Genre:\t\t\t %s\nSprache(n):\t\t %s\n" 
                    % (x[4], x[5]))
                break
            except ValueError:
                 print("Noch nichts gefunden, weiter…")
        datei.close()
        if not gefunden:
            print("Leider nichts Passendes gefunden.")
        '''
        for x in infos:
            datei.write(x[0] + "|" + x[1] + "|" + x[2] + "|" + x[3] + "|" + x[4] + "|" + x[5] + "\n")
        datei.flush()'''
    finally:
        datei.close()
    input("ENTER drücken zum Fortfahren")
    hauptmenu()


def zeige_bestand():
    os.system("clear")
    wahl1 = None
    try:
        wahl1 = input("Möchten Sie eine ausführliche Liste (A) oder nur die Titel mit Autor dazu (T)?\t")
    except:
        print("\nERROR! Fehlerhafte Eingabe oder Unterbrechung.\n")
        exit()
    
    wahl2 = None
    try:
        wahl2 = input("Ausgabe auf den Bildschirm (B) oder in eine Datei namens bestand.txt (D)?\t")
    except:
        print("\nERROR! Fehlerhafte Eingabe oder Unterbrechung.\n")
        exit()

    if wahl1 == 'A' and wahl2 == 'B':
        print(rahmenmacher("Bestand ausführlich:"))
        try:
            datei = open("libfile.txt", "r+")
            inhalt = datei.readlines()
            inhalt = [x.strip() for x in inhalt] # for end-whitespaces and '\n' removal ;)
            
            infos = list()
            for x in range(0,len(inhalt)):
                eintrag = (inhalt[x].split('|', maxsplit=5)) # vorher maxsplit=6 wegen refno!!!
                infos.append(eintrag)
                print("Autor:\t\t\t %s\nTitel:\t\t\t %s\nVerlag:\t\t\t %s\nErscheinungsjahr:\t %s\n"
                        % (infos[x][0], infos[x][1], infos[x][2], infos[x][3]), end="")
                print("Genre:\t\t\t %s\nSprache(n):\t\t %s\n" 
                    % (infos[x][4], infos[x][5]))
        finally:
            datei.close()
        input("\nENTER drücken zum Fortfahren")
        hauptmenu()
    elif wahl1 == 'A' and wahl2 == 'D':
        try:
            datei = open("libfile.txt", "r+")
            inhalt = datei.readlines()
            inhalt = [x.strip() for x in inhalt] # for whitespace and '\n' removal ;)
            
            datei_raus = open("bestand.txt", "w+")
            datei_raus.write(rahmenmacher("Bestand ausführlich:"))

            infos = list()
            for x in range(0,len(inhalt)):
                eintrag = (inhalt[x].split('|', maxsplit=5)) # vorher maxsplit=6 wegen refno!!!
                infos.append(eintrag)
                datei_raus.write("Autor:\t\t\t " + infos[x][0] + "\nTitel:\t\t\t " + infos[x][1] + 
                        "\nVerlag:\t\t\t " + infos[x][2] + "\nErscheinungsjahr:\t " + infos[x][3] + "\n")
                ''' ALT:   print("Genre:\t\t\t %s\nSprache(n):\t\t %s\nInterne Referenznummer:\t %s\n" 
                    % (infos[x][4], infos[x][5], infos[x][6]))'''
                datei_raus.write("Genre:\t\t\t " + infos[x][4] + "\nSprache(n):\t\t " + infos[x][5] + "\n\n")
                datei_raus.flush()
        finally:
            datei_raus.close()
            datei.close()
        input("\nENTER drücken zum Fortfahren")
        hauptmenu()
    elif wahl1 == 'T' and wahl2 == 'B':
        print(rahmenmacher("Bestand, nur Autor & Titel:"))
        try:
            datei = open("libfile.txt", "r+")
            inhalt = datei.readlines()
            inhalt = [x.strip() for x in inhalt] # for whitespace and '\n' removal ;)
            
            infos = list()
            for x in range(0,len(inhalt)):
                eintrag = (inhalt[x].split('|', maxsplit=5)) # vorher maxsplit=6 wegen refno!!!
                infos.append(eintrag)
                print("Autor:\t\t\t %s\nTitel:\t\t\t %s\n" % (infos[x][0], infos[x][1]))
        finally:
            datei.close()
        input("\nENTER drücken zum Fortfahren")
        hauptmenu()
    elif wahl1 == 'T' and wahl2 == 'D':
        try:
            datei = open("libfile.txt", "r+")
            inhalt = datei.readlines()
            inhalt = [x.strip() for x in inhalt] # for whitespace and '\n' removal ;)
            
            datei_raus = open("bestand.txt", "w+")
            datei_raus.write(rahmenmacher("Bestand ausführlich:"))

            infos = list()
            for x in range(0,len(inhalt)):
                eintrag = (inhalt[x].split('|', maxsplit=5)) # vorher maxsplit=6 wegen refno!!!
                infos.append(eintrag)
                datei_raus.write("Autor:\t\t\t " + infos[x][0] + "\nTitel:\t\t\t " + infos[x][1] + "\n\n")
                datei_raus.flush()
        finally:
            datei_raus.close()
            datei.close()
        input("\nENTER drücken zum Fortfahren")
        hauptmenu()
    else:
        print("ERROR! Quitting...")
        exit()


def del_buch():
    print(rahmenmacher("Buch löschen:"))
    try:
        datei = open("libfile.txt", "r+")
        inhalt = datei.readlines()
        inhalt = [x.strip() for x in inhalt] # for end-whitespaces and '\n' removal ;)
        
        infos = list()
        for x in range(0,len(inhalt)):
            eintrag = (inhalt[x].split('|', maxsplit=5)) # vorher maxsplit=6 wegen refno!!!
            infos.append(eintrag)

        autoren, titel, verlage, ejahre, genres, sprachen = [], [], [], [], [], []
        for x in infos:
            autoren.append(x[0])
            titel.append(x[1])
            verlage.append(x[2])
            ejahre.append(x[3])
            genres.append(x[4])
            sprachen.append(x[5])
        # print(autoren, titel, verlage, ejahre, genres, sprachen, sep="\n", end="\n")
        
        # print("Bitte sicherheitshalber nur einen Suchbegriff (am besten den Titel) eingeben, und das genau wie in der Bestandsübersicht!")
        titel_such = input("Genauen Titel eingeben:\t\t")
        if titel_such == "":
            wahl = input("Noch die anderen Infos eingeben? (J/n)\t")
            if wahl == 'J':
                #autor_such = input("Autor(en) eingeben:\t\t") # nicht sinnvoll
                #verlag_such = input("genauen Verlag eingeben:\t") # nicht sinnvoll
                ejahr_such = input("Erscheinungsjahr eingeben:\t") 
                #genre_such = input("Genre eingeben:\t\t\t") # nicht sinnvoll
                #sprache_such = input("Sprache(n) eingeben:\t\t") # nicht sinnvoll
                #print("", autor_such, titel_such, verlag_such, ejahr_such, genre_such, sprache_such, sep="\n", end="\n")

        gefunden = False
        for x in infos:
            try:
                gesucht = x.index(titel_such) # stupid stub just for checkup
                gefunden = True
                titel_entfernt = x[gesucht]
                infos.remove(x)
                print("\nBuch mit dem Titel " + titel_entfernt + " erfolgreich entfernt.")
                break
            except ValueError:
                print("Noch nichts gefunden, weiter…")
        if not gefunden:
            print("Leider nichts Passendes gefunden, also auch nichts entfernt.")
        datei.close()
        del datei
        datei = open("libfile.txt", "w")
        for x in infos:
            datei.write(x[0] + "|" + x[1] + "|" + x[2] + "|" + x[3] + "|" + x[4] + "|" + x[5] + "\n")
        datei.flush()
    finally:
        datei.close()
    input("\nENTER drücken zum Fortfahren")
    hauptmenu()


def main():
    os.system("clear")
    hauptmenu()


if __name__ == "__main__":
    main()

'''
TODO:
    * mehr-Reizwort-Support (→Erscheinungsjahr) bei such_buch und del_buch implementieren
'''
